cmake_minimum_required(VERSION 3.8)

project(
  algo
)

set(CMAKE_CXX_VERSION 23)
set (CMAKE_CXX_STANDARD 23)

option(PRECISE_TIMING "Disable output for precise timing" OFF)
if(PRECISE_TIMING)
    add_definitions(-DPRECISE_TIMING)
endif()

include_directories ("${PROJECT_SOURCE_DIR}/types")
add_library(types src/types/ref_ptr.h)
set_target_properties(types PROPERTIES LINKER_LANGUAGE CXX)

include_directories ("${PROJECT_SOURCE_DIR}/collections")
add_library(collections src/collections/darray.h src/collections/heap.h src/collections/graph.h src/collections/dset.h)
target_link_libraries(collections PRIVATE types)
set_target_properties(collections PROPERTIES LINKER_LANGUAGE CXX)

include_directories ("${PROJECT_SOURCE_DIR}/algo")
add_library(algo_lib src/algo/algorithms.h)
target_link_libraries(algo_lib PRIVATE collections)
set_target_properties(algo_lib PROPERTIES LINKER_LANGUAGE CXX)

add_executable(algo src/main.cpp)
target_link_libraries(algo PRIVATE collections algo_lib)
