#include <concepts>
#include <iostream>

#pragma once

struct RefCounter {
  size_t count;
  bool is_array;
};

template <class T>
concept Fundamental = std::is_fundamental<T>::value;
template <typename T> class RefPtr {
public:
  RefPtr(T *ptr = nullptr, size_t size = 0);
  RefPtr(RefPtr<T> &other);
  RefPtr(RefPtr<T> &&other);
  ~RefPtr();
  RefPtr<T> at(size_t s);
  void operator=(RefPtr<T> other);
  T *operator->();
  T operator[](size_t i) const;
  T &operator[](size_t i);
  template <typename U>
  operator RefPtr<U>() const
    requires std::derived_from<T, U>;
  template <class... Args>
  static RefPtr<T> make(size_t size, Args &&...args)
    requires Fundamental<T>;
  template <class... Args> static RefPtr<T> make(size_t size, Args &&...args);
  bool is_array();
  bool is_null();
  RefPtr(T *data, RefCounter *ref, size_t start);

protected:
  RefCounter *ref;
  size_t start = 0;
  T *p_data = nullptr;
};

template <typename T>
RefPtr<T>::RefPtr(T *data, RefCounter *ref, size_t start) {
  this->p_data = data;
  this->ref = ref;
  this->ref->count += 1;
  this->start = start;
}
template <typename T>
template <typename U>
RefPtr<T>::operator RefPtr<U>() const
  requires std::derived_from<T, U>
{
  return RefPtr<U>(static_cast<U *>(p_data), ref, start);
}

template <typename T> RefPtr<T> RefPtr<T>::at(size_t s) {
  RefPtr<T> ret((*this));
  ret.start += s;
  return ret;
}
template <typename T> bool RefPtr<T>::is_array() { return ref->is_array; }
template <typename T> bool RefPtr<T>::is_null() { return p_data == nullptr; }

template <typename T> RefPtr<T>::RefPtr(T *ptr, size_t size) {
  ref = new RefCounter();
  ref->count = 1;
  ref->is_array = (bool)size;
  p_data = ptr;
}

template <typename T> RefPtr<T>::RefPtr(RefPtr<T> &other) {
  ref = other.ref;
  ref->count++;
  p_data = other.p_data;
  start = other.start;
}

template <typename T> RefPtr<T>::RefPtr(RefPtr<T> &&other) {
  ref = other.ref;
  ref->count += 1;
  p_data = other.p_data;
  start = other.start;
  other.ref = 0;
  other.p_data = 0;
}

template <typename T> RefPtr<T>::~RefPtr() {
  if (!p_data)
    return;
  ref->count--;
  if (ref->count != 0)
    return;

  if (ref->is_array)
    delete[] p_data;
  else
    delete p_data;

  delete ref;
  return;
}

template <typename T> T RefPtr<T>::operator[](size_t i) const {
  return p_data[start + i];
}
template <typename T> T &RefPtr<T>::operator[](size_t i) {
  return p_data[start + i];
}

template <typename T> void RefPtr<T>::operator=(RefPtr<T> other) {
  this->~RefPtr();
  ref = other.ref;
  ref->count++;
  p_data = other.p_data;
  start = other.start;
}
template <typename T> T *RefPtr<T>::operator->() { return p_data; }

template <typename T>
template <class... Args>
RefPtr<T> RefPtr<T>::make(size_t size, Args &&...args)
  requires Fundamental<T>
{
  if (size) {
    return RefPtr<T>(new T[size], size);
  }
  return RefPtr<T>(new T(args...));
}

template <typename T>
template <class... Args>
RefPtr<T> RefPtr<T>::make(size_t size, Args &&...args) {
  if (size) {
    return RefPtr<T>(new T[size], size);
  }
  return RefPtr<T>(new T(args...));
}
