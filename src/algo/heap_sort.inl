#include "../collections/heap.h"
#include <functional>

template <typename T> void heap_sort(DArray<T> to_sort, std::function<bool(T, T)> p) {
  // create heap based on DArray
  Heap<T> h(to_sort, p);
  while (h.get_size() > 0) {
    // put extracted maximum element of the heap at 
    // the freed position at the end of heap
    size_t tmp = h.get_size();
    to_sort[tmp - 1] = h.extract();
  }
}
