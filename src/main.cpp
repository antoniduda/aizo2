#include "collections/graph.h"
#include <chrono>
#include <fstream>
#include <iostream>
#include <stdint.h>

#define TIMEIT(f, ...)                                                         \
  {                                                                            \
    const auto start = std::chrono::high_resolution_clock::now();              \
    f(__VA_ARGS__);                                                            \
    const auto end = std::chrono::high_resolution_clock::now();                \
    const std::chrono::duration<long int, std::nano> diff = end - start;       \
    std::cout << #f << ' ' << diff.count() << "ns\n\n";                                    \
  }

RefPtr<GraphAdjList> list;
RefPtr<GraphAdjMatrix> matrix;
int g_choice;
void generate() {
  size_t density;
  size_t v;
  std::cout << "Podaj gęstość w procentach: ";
  std::cin >> density;
  std::cout << "Podaj liczbę wierzchołków: ";
  std::cin >> v;
  list = Graph::generate<GraphAdjList>(density, v);
  matrix = RefPtr<GraphAdjMatrix>::make(0, v);
  matrix->add_edges_from_graph(list);
}

void read_file() {
  std::string path;
  std::cout << "Podaj ścieżkę do pliku: ";
  std::cin >> path;
  std::fstream myfile(path, std::ios_base::in);
  size_t e, v;
  myfile >> e >> v;
  list = RefPtr<GraphAdjList>::make(0, v);
  matrix = RefPtr<GraphAdjMatrix>::make(0, v);

  for (size_t i = 0; i < e; i++) {
    size_t start, end, w;
    myfile >> start >> end >> w;
    list->add_edge(start, end, w);
    matrix->add_edge(start, end, w);
  }
}

int main() {
  while (true) {
    std::cout << "1. Wygeneruj graf\n";
    std::cout << "2. Wyświetl graf\n";
    std::cout << "3. Algorytm Dijkstry\n";
    std::cout << "4. Algorytm BellmanaForda\n";
    std::cout << "5. Algorytm Prima\n";
    std::cout << "6. Algorytm Kruskala\n";
    std::cout << "7. Algorytm Forda-Fulkersona\n";
    std::cout << "8. Zaczytaj plik\n";
    int tmp;
    size_t from;
    size_t to;
    std::cin >> tmp;
    switch (tmp) {
    case 1:
      generate();
      break;
    case 2:
      std::cout << "Wypisać w postaci listy (1), czy macieży (2)? ";
      std::cin >> g_choice;
      if (g_choice == 1)
        list->display();
      else
        matrix->display();
      break;
    case 3:
      std::cout << "Podaj wierzchołek początkowy ";
      std::cin >> from;
      std::cout << "Podaj wierzchołek końcowy ";
      std::cin >> to;
      TIMEIT(list->dijkstra, from, to);
      TIMEIT(matrix->dijkstra, from, to);
      break;
    case 4:
      std::cout << "Podaj wierzchołek początkowy ";
      std::cin >> from;
      std::cout << "Podaj wierzchołek końcowy ";
      std::cin >> to;
      TIMEIT(list->bellman_ford, from, to);
      TIMEIT(matrix->bellman_ford, from, to);
      break;
    case 5:
      TIMEIT(list->prim);
      TIMEIT(matrix->prim);
      break;
    case 6:
      TIMEIT(list->kruskal);
      TIMEIT(matrix->kruskal);
      break;
    case 7:
      std::cout << "Podaj wierzchołek początkowy ";
      std::cin >> from;
      std::cout << "Podaj wierzchołek końcowy ";
      std::cin >> to;
      TIMEIT(list->ford_fulkerson, from, to);
      TIMEIT(matrix->ford_fulkerson, from, to);
      break;
    case 8:
      read_file();
      break;
    }
  }
}
