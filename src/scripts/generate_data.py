#!/bin/env python
import pwn

io = pwn.process("../../build/algo")

def main_menu_opt(i):
    io.sendlineafter(b"8. Zaczytaj plik\n", str(i).encode())

def generate_graph(size, density):
    main_menu_opt(1)
    io.sendlineafter("Podaj gęstość w procentach: ".encode(), str(density).encode())
    io.sendlineafter("Podaj liczbę wierzchołków: ".encode(), str(size).encode())

def dijkstra(start, end):
    main_menu_opt(3)
    io.sendlineafter("Podaj wierzchołek początkowy ".encode(), str(start).encode())
    io.sendlineafter("Podaj wierzchołek końcowy ".encode(), str(end).encode())
    io.recvuntil(b"list->dijkstra")
    time_list = int(io.recvline()[:-3])
    io.recvuntil(b"matrix->dijkstra")
    time_matrix = int(io.recvline()[:-3])
    return time_list, time_matrix

def bellman_ford(start, end):
    main_menu_opt(4)
    io.sendlineafter("Podaj wierzchołek początkowy ".encode(), str(start).encode())
    io.sendlineafter("Podaj wierzchołek końcowy ".encode(), str(end).encode())
    io.recvuntil(b"list->bellman_ford")
    time_list = int(io.recvline()[:-3])
    io.recvuntil(b"matrix->bellman_ford")
    time_matrix = int(io.recvline()[:-3])
    return time_list, time_matrix

def prim():
    main_menu_opt(5)
    io.recvuntil(b"list->prim")
    time_list = int(io.recvline()[:-3])
    io.recvuntil(b"matrix->prim")
    time_matrix = int(io.recvline()[:-3])
    return time_list, time_matrix

def kruskal():
    main_menu_opt(5)
    io.recvuntil(b"list->prim")
    time_list = int(io.recvline()[:-3])
    io.recvuntil(b"matrix->prim")
    time_matrix = int(io.recvline()[:-3])
    return time_list, time_matrix

def ford_fulkerson(start, end):
    main_menu_opt(7)
    io.sendlineafter("Podaj wierzchołek początkowy ".encode(), str(start).encode())
    io.sendlineafter("Podaj wierzchołek końcowy ".encode(), str(end).encode())
    io.recvuntil(b"list->ford_fulkerson")
    time_list = int(io.recvline()[:-3])
    io.recvuntil(b"matrix->ford_fulkerson")
    time_matrix = int(io.recvline()[:-3])
    return time_list, time_matrix


def dump_line(algo, size, density, l, m):
    with open('output.csv', 'a') as f:
        f.write(f"{algo}, {size}, {density}, {l}, {m}\n")

densities = [25, 50, 99]
sizes = [40, 60, 80, 100, 120, 140, 160]

for size in sizes:
    print(size)
    for density in densities:
        print(density)
        for i in range(100):
            generate_graph(size, density)
            l, m = dijkstra(0, size-1)
            dump_line("dijkstra", size, density, l, m)
            l, m = bellman_ford(0, size-1)
            dump_line("bellman_ford", size, density, l, m)
            l, m = ford_fulkerson(0, size-1)
            dump_line("ford_fulkerson", size, density, l, m)
            l, m = prim()
            dump_line("prim", size, density, l, m)
            l, m = kruskal()
            dump_line("kruskal", size, density, l, m)

