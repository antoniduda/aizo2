#include <iomanip>
GraphAdjMatrix::GraphAdjMatrix(size_t v) : Graph(v) {
  adj_matrix.ensure_capacity(v);
  for (size_t i = 0; i < v; i++) {
    adj_matrix.push_back(DArray<RefPtr<Edge>>(v));
  }
}

void GraphAdjMatrix::add_edge(size_t from, size_t to, size_t weigth) {
  Graph::add_edge(from, to, weigth);
  adj_matrix[from][to] = _edges[_edges.get_size() - 1];
}

std::generator<RefPtr<Edge> &> GraphAdjMatrix::edges(size_t v) {
  for (size_t i = 0; i < adj_matrix[v].get_size(); i++) {
    if (!adj_matrix[v][i].is_null())
      co_yield adj_matrix[v][i];
  }
  for (size_t i = 0; i < adj_matrix[v].get_size(); i++) {
    if (!adj_matrix[i][v].is_null())
      co_yield adj_matrix[i][v];
  }
}

std::generator<RefPtr<Edge> &> GraphAdjMatrix::out_edges(size_t v) {
  for (size_t i = 0; i < adj_matrix[v].get_size(); i++) {
    if (!adj_matrix[v][i].is_null())
      co_yield adj_matrix[v][i];
  }
}

std::generator<RefPtr<Edge> &> GraphAdjMatrix::in_edges(size_t v) {
  for (size_t i = 0; i < adj_matrix[v].get_size(); i++) {
    if (!adj_matrix[i][v].is_null())
      co_yield adj_matrix[i][v];
  }
}

size_t GraphAdjMatrix::get_size() { return adj_matrix.get_size(); }
void GraphAdjMatrix::display() {
  for (size_t i = 0; i < get_size(); i++) {
    for (size_t j = 0; j < get_size(); j++) {
        if (adj_matrix[i][j].is_null()) {
            std::cout << "XXXX ";
            continue;
        }
        std::cout << std::setw(4) << adj_matrix[i][j]->weigth << " ";
    }
    std::cout<< "\n";
  }
}
