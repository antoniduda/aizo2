
#include <stdint.h>
#include <iostream>
#include "darray.h"

#pragma once

class DSet {
public:
  DSet(size_t n);
  size_t find_parent(size_t a);
  void union_(size_t a, size_t b);
private:
  DArray<size_t> parents;
  DArray<size_t> rank;
};

#include "dset.inl"
