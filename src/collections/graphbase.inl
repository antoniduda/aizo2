#include "../algo/algorithms.h"
#include "darray.h"
#include "dset.h"
#include <limits>
#include <random>

Graph::Graph(size_t v) { this->v = v; };

void Graph::add_edges_from_graph(RefPtr<Graph> g)
{
    this->v = g->v;
    for(size_t i = 0; i < g->_edges.get_size(); i++) {
        auto edge = g->_edges[i];
        this->add_edge(edge->from, edge->to, edge->weigth);
    }
};

template <typename T> RefPtr<T> Graph::generate(uint8_t density, size_t v) {
  DArray<bool> used(v);
  for (size_t i = 0; i < used.get_size(); i++) {
    used[i] = false;
  }
  auto g = RefPtr<T>::make(0, v);

  std::random_device dev;
  std::mt19937 rng(dev());
  std::uniform_int_distribution<std::mt19937::result_type> distv(0, v - 1);
  std::uniform_int_distribution<std::mt19937::result_type> distm(0, 1000);

  size_t start_vs = distv(rng);
  for (size_t i = 0; i < v - 1; i++) {
    used[start_vs] = true;
    size_t vs;
    do {
      vs = distv(rng);
    } while (used[vs] == true);
    size_t weigth = distm(rng);
    g->add_edge(start_vs, vs, weigth);
    start_vs = vs;
  }
  size_t max_edge_count = v * (v - 1);
  size_t count = v - 1;
  while (count * 100 / max_edge_count < density) {
    size_t from_vs = distv(rng);
    size_t to_vs = distv(rng);
    if (g->check_edge_dup(from_vs, to_vs))
      continue;
    g->add_edge(from_vs, to_vs, distm(rng));
    count++;
  }
  return g;
}

void Graph::add_edge(size_t from, size_t to, size_t weight) {
  _edges.push_back(RefPtr<Edge>::make(0, from, to, weight));
}

bool Graph::check_edge_dup(size_t from_vs, size_t to_vs) {
  if (from_vs == to_vs)
    return true;

  for (auto edge : edges(from_vs)) {
    if (edge->from == from_vs && edge->to == to_vs)
      return true;
  }
  return false;
}

void Graph::dijkstra(size_t v, size_t to) {
  DArray<size_t> dis(get_size());
  DArray<RefPtr<Edge>> edges(get_size());
  for (size_t i = 0; i < get_size(); i++)
    dis[i] = std::numeric_limits<size_t>::max();

  using namespace std::placeholders;
  std::function<bool(size_t, size_t)> p = std::bind(property, &dis, _1, _2);
  Heap<size_t> h(p);
  dis[v] = 0;
  h.insert(v);
  while (h.get_size() > 0) {
    size_t best = h.extract();
    if (best == to)
      break;
    for (auto edge : out_edges(best)) {
      if (dis[best] + edge->weigth < dis[edge->to]) {
        dis[edge->to] = dis[best] + edge->weigth;
        edges[edge->to] = edge;
        h.insert(edge->to);
      }
    }
  }
#ifndef PRECISE_TIMING
  size_t c = to;
  std::cout << "Ścieżka: ";
  while (c != v) {
    if (edges[c].is_null()) {
      std::cout << "\nAlgorytm nie dotarł do wierzchołka końcowego\n";
      return;
    }
    std::cout << c << " <- ";
    c = edges[c]->from;
  }
  std::cout << v << "\n";
  std::cout << "Długość ścieżki " << dis[to] << "\n";
#endif
}

void Graph::bellman_ford(size_t v, size_t to) {
  DArray<size_t> dis(get_size());
  DArray<bool> displayed(get_size());
  DArray<RefPtr<Edge>> edges(get_size());
  for (size_t i = 0; i < get_size(); i++) {
    dis[i] = std::numeric_limits<size_t>::max();
    displayed[i] = false;
  }

  dis[v] = 0;
  for (int i = 0; i < get_size() - 1; i++) {
    for (int j = 0; j < _edges.get_size(); j++) {
      size_t tmp = dis[_edges[j]->from] + _edges[j]->weigth;
      if (tmp < dis[_edges[j]->to]) {
        dis[_edges[j]->to] = tmp;
        edges[_edges[j]->to] = _edges[j];
      }
    }
  }
#ifndef PRECISE_TIMING
  size_t c = to;
  std::cout << "Ścieżka: ";
  while (c != v) {
    if (edges[c].is_null() || displayed[c]) {
      std::cout << "\nAlgorytm nie dotarł do wierzchołka końcowego\n";
      return;
    }
    std::cout << c << " <- ";
    displayed[c] = true;
    c = edges[c]->from;
  }
  std::cout << v << "\n";
  std::cout << "Długość ścieżki " << dis[to] << "\n";
#endif
}

void Graph::prim() {
  DArray<bool> visited(get_size());
  for (size_t i = 0; i < get_size(); i++)
    visited[i] = false;

  using namespace std::placeholders;
  Heap<RefPtr<Edge>> h([](RefPtr<Edge> a, RefPtr<Edge> b) -> bool {
    return b->weigth < a->weigth;
  });
  size_t v = 0;
  for (size_t i = 0; i < get_size(); i++) {
    visited[v] = true;
    for (auto edge : edges(v)) {
      if (visited[edge->from] && visited[edge->to])
        continue;
      h.insert(edge);
    }
    if (h.get_size() == 0)
      return;
    auto best = h.extract();
    size_t tmp;
    tmp = best->from;
    if (visited[best->from])
      tmp = best->to;
    if (visited[tmp])
      continue;
    v = tmp;
#ifndef PRECISE_TIMING
    std::cout << best->from << " - " << best->to << '\n';
#endif
  }
}

void Graph::kruskal() {
  heap_sort<RefPtr<Edge>>(_edges, [](RefPtr<Edge> a, RefPtr<Edge> b) -> bool {
    return b->weigth > a->weigth;
  });
  DSet set(get_size());
  for (size_t i = 0; i < _edges.get_size(); i++) {
    if (set.find_parent(_edges[i]->from) == set.find_parent(_edges[i]->to))
      continue;
    set.union_(_edges[i]->from, _edges[i]->to);
#ifndef PRECISE_TIMING
    std::cout << _edges[i]->from << " - " << _edges[i]->to << "\n";
#endif
  }
}

void Graph::ford_fulkerson(size_t from, size_t to) {
  DArray<bool> visited(get_size());
  size_t total = 0;
  for (auto edge : in_edges(to))
    total += edge->weigth;
  size_t current;
  size_t res = 0;
  do {
    for (size_t i = 0; i < get_size(); i++)
      visited[i] = false;

    current = ford_fulkerson_rec(visited, total, from, to);
    res += current;
  } while (current != 0);
#ifndef PRECISE_TIMING
  std::cout << "Maks przepływ " << res << '\n';
#endif
};

size_t Graph::ford_fulkerson_rec(DArray<bool> visited, size_t max_score,
                                 size_t c, size_t to) {
  if (c == to) {
    return max_score;
  }
  visited[c] = true;

  size_t usable;
  size_t max_val;
  size_t tmp = 0;
  for (auto edge : edges(c)) {
    usable = edge->weigth - edge->used;
    if (max_score == 0)
      break;
    if (edge->from == c && usable && !visited[edge->to]) {
      // out edge
      max_val = max_score < usable ? max_score : usable;
      tmp = ford_fulkerson_rec(visited, max_val, edge->to, to);
      edge->used += tmp;
    } else if (edge->used && !visited[edge->from] && edge->from != c) {
      // in edge
      max_val = max_score < edge->used ? max_score : edge->used;
      tmp = ford_fulkerson_rec(visited, max_val, edge->from, to);
      edge->used -= tmp;
    }
    if (tmp != 0)
      return tmp;
  }
  return 0;
}

bool property(DArray<size_t> dis, size_t a, size_t b) {
  return dis[b] < dis[a];
}
