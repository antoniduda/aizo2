GraphAdjList::GraphAdjList(size_t v) : Graph(v) {
  adj_list = DArray<DArray<RefPtr<Edge>>>(v);
}

void GraphAdjList::add_edge(size_t from, size_t to, size_t weigth) {
  Graph::add_edge(from, to, weigth);
  adj_list[from].push_back(_edges[_edges.get_size() - 1]);
  adj_list[to].push_back(_edges[_edges.get_size() - 1]);
}

std::generator<RefPtr<Edge> &> GraphAdjList::edges(size_t v) {
  for (size_t i = 0; i < adj_list[v].get_size(); i++)
    co_yield adj_list[v][i];
}

std::generator<RefPtr<Edge> &> GraphAdjList::out_edges(size_t v) {
  for (size_t i = 0; i < adj_list[v].get_size(); i++) {
    if (adj_list[v][i]->from == v)
      co_yield adj_list[v][i];
  }
}

std::generator<RefPtr<Edge> &> GraphAdjList::in_edges(size_t v) {
  for (size_t i = 0; i < adj_list[v].get_size(); i++) {
    if (adj_list[v][i]->to == v)
      co_yield adj_list[v][i];
  }
}

size_t GraphAdjList::get_size() { return adj_list.get_size(); }
void GraphAdjList::display() {
  for (size_t i = 0; i < get_size(); i++) {
    std::cout << i << ": ";
    for (auto edge : out_edges(i)) {
      std::cout << edge->to << " ";
    }
    std::cout << "\n";
  }
}
