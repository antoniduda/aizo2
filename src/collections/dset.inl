#include "darray.h"

DSet::DSet(size_t n) {
  parents.resize(n);
  rank.resize(n);
  for (size_t i = 0; i < n; i++) {
    rank[i] = 1;
    parents[i] = i;
  }
}

size_t DSet::find_parent(size_t a) {
  while (this->parents[a] != a)
    a = this->parents[a];
  return a;
}

void DSet::union_(size_t a, size_t b) {
  size_t a_p = find_parent(a);
  size_t b_p = find_parent(b);
  if (a_p == b_p)
    return;
  if (rank[a_p] < rank[b_p])
    parents[a_p] = b_p;
  else if (rank[a_p] > rank[b_p])
    parents[b_p] = a_p;
  else {
    parents[a_p] = b_p;
    rank[b_p]++;
  }
}
