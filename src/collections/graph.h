#include "../types/ref_ptr.h"
#include "darray.h"
#include "heap.h"
#include <generator>
#include <limits>
#include <concepts>
#include <stdint.h>

#pragma once

struct Edge {
  size_t from;
  size_t to;
  size_t weigth;
  size_t used = 0;
};

class Graph {
public:
  Graph(){};
  Graph(size_t v);
  virtual void add_edges_from_graph(RefPtr<Graph> g);
  template <typename T> static RefPtr<T> generate(uint8_t density, size_t v);
  static RefPtr<Graph> generate_not_dir(uint8_t density, size_t v);
  virtual std::generator<RefPtr<Edge> &> in_edges(size_t v) = 0;
  virtual std::generator<RefPtr<Edge> &> out_edges(size_t v) = 0;
  virtual std::generator<RefPtr<Edge> &> edges(size_t v) = 0;
  virtual size_t get_size() = 0;
  virtual void add_edge(size_t from, size_t to, size_t weight);
  virtual void display() = 0;
  void dijkstra(size_t from, size_t to);
  void bellman_ford(size_t from, size_t to);
  void ford_fulkerson(size_t from, size_t to);
  void prim();
  void kruskal();

protected:
  size_t v;
  bool check_edge_dup(size_t from_vs, size_t to_vs);
  size_t ford_fulkerson_rec(DArray<bool> visited, size_t max_score, size_t c, size_t to);
  DArray<RefPtr<Edge>> _edges;
};

bool property(DArray<size_t> dis, size_t a, size_t b);

class GraphAdjList : public Graph {
public:
  GraphAdjList(){};
  GraphAdjList(size_t v);
  void add_edge(size_t from, size_t to, size_t weigth) override;
  virtual std::generator<RefPtr<Edge> &> edges(size_t v) override;
  virtual std::generator<RefPtr<Edge> &> out_edges(size_t v) override;
  virtual std::generator<RefPtr<Edge> &> in_edges(size_t v) override;
  virtual void display() override;
  virtual size_t get_size() override;

private:
  DArray<DArray<RefPtr<Edge>>> adj_list;
};

class GraphAdjMatrix : public Graph {
public:
  GraphAdjMatrix(){};
  GraphAdjMatrix(size_t v);
  void add_edge(size_t from, size_t to, size_t weigth) override;
  virtual std::generator<RefPtr<Edge> &> edges(size_t v) override;
  virtual std::generator<RefPtr<Edge> &> out_edges(size_t v) override;
  virtual std::generator<RefPtr<Edge> &> in_edges(size_t v) override;
  virtual void display();
  virtual size_t get_size();

private:
  DArray<DArray<RefPtr<Edge>>> adj_matrix;
};

#include "graphadjlist.inl"
#include "graphadjmatrix.inl"
#include "graphbase.inl"
