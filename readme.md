# Algorithms project for my university course

A project presenting implementation of multiple graph algorithms including:
- Dijkstra's algorithm
- Bellman - Ford algorithm
- Prim's algorithm
- Kruskal's algorithm

In order to code the algorithms following data structures were implemented:
- DArray - dynamic array a replacement for Vector container
- Heap - used as a priority queue in several algorithms and for sorting in Kruskal's algorithm
- DSet - disjoint set used for determinig whether there are cycles in a graph
- RefPtr - A reference counting pointer which frees resources when all the references get destroyed

The algorithms were implemented both for adjacency matrix and adjacency list.
